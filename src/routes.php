<?php
// Routes

// $app->get('/[{name}]', function ($request, $response, $args) {
//     $this->logger->info("Slim-Skeleton '/' route");
//     return $this->renderer->render($response, 'index.phtml', $args);
// });
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response->withHeader('Access-Control-Allow-Origin', 'http://localhost:8100')
    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type, Accept, Origin, Authorization')
    ->withHeader('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
});

//Register new user
$app->post('/v1/register', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $info = array();
    //Check if user exists
    $checkemail = "SELECT * FROM tbl_user_accounts WHERE ua_email=:email";
    $checkemail = $this->db->prepare($checkemail);
    $checkemail->bindValue("email", $data['email']);
    $checkemail->execute();
    $isregistered = $checkemail->fetchObject();

    //Check if username is taken
    $checkuser = "SELECT * FROM tbl_user_accounts WHERE ua_username=:username";
    $checkuser = $this->db->prepare($checkuser);
    $checkuser->bindValue("username", $data['uname']);
    $checkuser->execute();
    $istaken = $checkuser->fetchObject();

    if ($isregistered) {
        //Error message
        $info['success'] = false;
        $info['error']  = 'Sorry! This email has already been registered.';
    } elseif ($istaken) {
        //Error message
        $info['success'] = false;
        $info['error']  = 'Sorry! This username is taken.';
    //Check if passwords match
    } elseif ($data['pass'] != $data['cpass']) {
        //Error message
        $info['success'] = false;
        $info['error']  = 'Passwords do not match.';
    } else {
        //Register new user
        $insertuser="INSERT INTO tbl_user_accounts (ua_name, ua_username, ua_email, ua_password, ua_sendnewsletter) VALUES (:name, :username, :email, :password, :newsletter)";
        $insertuser = $this->db->prepare($insertuser);
        $insertuser->bindValue("name", $data['fname']);
        $insertuser->bindValue("username", $data['uname']);
        $insertuser->bindValue("email", $data['email']);
        $insertuser->bindValue("password", md5($data['pass']));
        $insertuser->bindValue("newsletter", ($data['notify']) ? 1 : 0);
        $insertuser->execute();
        $userid = $this->db->lastInsertId();
        $info['success'] = true;
        $info['message'] = 'Successfully registered';
        $info['userid'] = $userid;
        $info['username'] = $data['username'];
        $info['name'] = $data['name'];
        $info['avi'] = 'assets/img/avatar.png';
    }
    return $this->response->withJson($info);
});

//Login registered user
$app->post('/v1/login', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $qry = "SELECT ua_id AS userid, ua_name AS name, ua_username as username, ua_avipath as avi FROM tbl_user_accounts WHERE (ua_email=:login OR ua_username=:login) AND ua_password=:password";
    $qry = $this->db->prepare($qry);
    $qry->bindValue("login", $data['login']);
    $qry->bindValue("password", md5($data['password']));
    $qry->execute();
    $user = $qry->fetchAll();
    if ($user) {
        $info['success'] = true;
        $info['message'] = 'Success';
        $info['user'] = $user;
    } else {
        $info['success'] = false;
        $info['message'] = 'Wrong username/email or password!';
    }
    return $this->response->withJson($info);
});

//Get all posts
$app->get('/v1/posts/user/{user}', function ($request, $response, $args) {
    $qry = "SELECT pp_id AS id, pp_type AS type, ua_name AS artiste, ua_avipath AS avi, pp_title AS title, pp_sourceURL AS src, pp_art AS art, pp_preload AS preload, pp_text AS text, pp_dateadded AS time, pp_likes AS likes, pp_comments AS comments, CASE pl_userid WHEN :user THEN 'true' ELSE 'false' END AS isliked FROM tbl_pod_posts PP LEFT JOIN tbl_user_accounts UA ON PP.pp_artisteid = UA.ua_id LEFT JOIN tbl_pod_likes PL ON PP.pp_id = PL.pl_postid WHERE PP.pp_isactive = 1 AND UA.ua_isactive = 1 ORDER BY pp_dateadded DESC";
    $qry = $this->db->prepare($qry);
    $qry->bindParam("user", $args['user']);
    $qry->execute();
    $posts = $qry->fetchAll();
    
    return $this->response->withJson($posts);
});

//Get all categories
$app->get('/v1/posts/categories', function ($request, $response, $args) {
    $qry = "SELECT pc_id AS id, pc_name AS title, pc_ionicon AS icon FROM tbl_pod_categories WHERE pc_isactive = 1 ORDER BY pc_name ASC";
    $qry = $this->db->prepare($qry);
    $qry->execute();
    $categories = $qry->fetchAll();
    return $this->response->withJson($categories);
});

//Get all posts from a specific category
$app->get('/v1/posts/category/{category}', function ($request, $response, $args) {
    $qry = "SELECT pp_id AS id, pp_type AS type, ua_name AS artiste, ua_avipath AS avi, pp_title AS title, pp_sourceURL AS src, pp_art AS art, pp_preload AS preload, pp_text AS text, pp_dateadded AS time, pp_likes AS likes FROM tbl_pod_posts PP LEFT JOIN tbl_user_accounts UA ON PP.pp_artisteid = UA.ua_id WHERE PP.pp_isactive = 1 AND UA.ua_isactive = 1 AND pp_categoryid = :category ORDER BY pp_dateadded DESC";
    $qry = $this->db->prepare($qry);
    $qry->bindParam("category", $args['category']);
    $qry->execute();
    $posts = $qry->fetchAll();
    return $this->response->withJson($posts);
});

//Like a post
$app->post('/v1/like', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $qry = "INSERT INTO tbl_pod_likes (pl_postid, pl_userid) VALUES (:post, :user)";
    $qry = $this->db->prepare($qry);
    $qry->bindValue("post", $data['post']);
    $qry->bindValue("user", $data['user']);
    $isliked = $qry->execute();
    if ($isliked) {
        $qry = $this->db->prepare("UPDATE tbl_pod_posts SET pp_likes = pp_likes + 1 WHERE pp_id=:post");
        $qry->bindValue("post", $data['post']);
        $qry->execute();
        $info['success'] = true;
        $info['message'] = 'Liked';
    } else {
        $info['success'] = false;
        $info['error']  = 'Already liked';
    }
    return $this->response->withJson($info);
});

//Remove like from a post
$app->delete('/v1/unlike/{post}/{user}', function ($request, $response, $args) {
    $qry = "DELETE FROM tbl_pod_likes WHERE pl_postid=:post AND pl_userid=:user";
    $qry = $this->db->prepare($qry);
    $qry->bindParam("post", $args['post']);
    $qry->bindParam("user", $args['user']);
    $unlike = $qry->execute();

    //Check if delete successful
    $cnt = $this->db->prepare("SELECT COUNT(*) AS cnt FROM tbl_pod_likes WHERE pl_postid=:post AND pl_userid=:user");
    $cnt->bindParam("post", $args['post']);
    $cnt->bindParam("user", $args['user']);
    $cnt->execute();
    $unlike = $cnt->fetch();

    if ($unlike) {
        $qry = $this->db->prepare("UPDATE tbl_pod_posts SET pp_likes = pp_likes - 1 WHERE pp_id=:post");
        $qry->bindValue("post", $args['post']);
        $qry->execute();
        $info['success'] = true;
        $info['message'] = 'Unliked';
    } else {
        $info['success'] = false;
        $info['error']  = 'Error. Please try again';
    }
    return $this->response->withJson($info);
});

//Todo
//Get post likers
$app->get('/v1/likes/{post}', function ($request, $response, $args) {
});

//Subscribe to a podcaster
$app->post('/v1/subscribe', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $qry = "INSERT INTO tbl_pod_subscriptions (ps_artisteid , ps_subscriberid) VALUES (:artiste, :subscriber)";
    $qry = $this->db->prepare($qry);
    $qry->bindValue("artiste", $data['artiste']);
    $qry->bindValue("subscriber", $data['subscriber']);
    $issubscribed = $qry->execute();
    if ($issubscribed) {
        $info['success'] = true;
        $info['message'] = 'Subscribed';
    } else {
        $info['success'] = false;
        $info['error']  = 'Error. Please try again';
    }
    return $this->response->withJson($info);
});

//Unsubscribe from a podcaster
$app->delete('/v1/unsubscribe/{artiste}/{subscriber}', function ($request, $response, $args) {
    $qry = "DELETE FROM tbl_pod_subscriptions WHERE ps_artisteid=:artiste AND ps_subscriberid=:subscriber";
    $qry = $this->db->prepare($qry);
    $qry->bindValue("artiste", $args['artiste']);
    $qry->bindValue("subscriber", $args['subscriber']);
    $qry->execute();

    //Check if delete successful
    $cnt = $this->db->prepare("SELECT COUNT(*) AS cnt FROM tbl_pod_subscriptions WHERE ps_artisteid=:artiste AND ps_subscriberid=:subscriber");
    $cnt->bindValue("artiste", $args['artiste']);
    $cnt->bindValue("subscriber", $args['subscriber']);
    $cnt->execute();
    $unsubscribe = $cnt->fetch();

    if ($unsubscribe['cnt'] == 0) {
        $info['success'] = true;
        $info['message'] = 'Unsubscribed';
    } else {
        $info['success'] = false;
        $info['error']  = 'Error. Please try again';
    }
    return $this->response->withJson($info);
});

//Todo
//Get podcast subscribers
$app->get('/v1/subscriptions/{podcast}', function ($request, $response, $args) {
});

//Comment on a post
$app->post('/v1/comment', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $qry = "INSERT INTO tbl_pod_comments (pc_postid, pc_text, pc_posterid) VALUES (:post, :comment, :user)";
    $qry = $this->db->prepare($qry);
    $qry->bindValue("post", $data['post']);
    $qry->bindValue("comment", $data['comment']);
    $qry->bindValue("user", $data['user']);
    $isposted = $qry->execute();
    if ($isposted) {
        $qry = $this->db->prepare("UPDATE tbl_pod_posts SET pp_comments = pp_comments + 1 WHERE pp_id=:post");
        $qry->bindValue("post", $data['post']);
        $qry->execute();
        $info['success'] = true;
        $info['message'] = 'Posted';
    } else {
        $info['success'] = false;
        $info['error']  = 'Comment not posted. Please try again';
    }
    return $this->response->withJson($info);
});

//Delete comment
$app->delete('/v1/uncomment/{comment}/{post}', function ($request, $response, $args) {
    $qry = "DELETE FROM tbl_pod_comments WHERE pc_id=:comment";
    $qry = $this->db->prepare($qry);
    $qry->bindValue("comment", $args['comment']);
    $qry->execute();

    //Check if delete successful
    $cnt = $this->db->prepare("SELECT COUNT(*) AS cnt FROM tbl_pod_comments WHERE pc_id=:comment");
    $cnt->bindValue("comment", $args['comment']);
    $cnt->execute();
    $deleted = $cnt->fetch();

    if ($deleted['cnt'] == 0) {
        $qry = $this->db->prepare("UPDATE tbl_pod_posts SET pp_comments = pp_comments - 1 WHERE pp_id=:post");
        $qry->bindValue("post", $args['post']);
        $qry->execute();
        $info['success'] = true;
        $info['message'] = 'Deleted';
    } else {
        $info['success'] = false;
        $info['error']  = 'Error. Please try again';
    }
    return $this->response->withJson($info);
});

//Get all comment for a specific post
$app->get('/v1/comments/[{post}]', function ($request, $response, $args) {
    $qry = "SELECT pc_id AS id, pc_text AS comment FROM tbl_pod_comments PC LEFT JOIN tbl_pod_posts PP ON PC.pc_postid = PP.pp_id LEFT JOIN tbl_user_accounts UA ON PC.pc_posterid = UA.ua_id WHERE PP.pp_isactive = 1 AND UA.ua_isactive = 1 AND pc_postid = :post ORDER BY pc_dateadded DESC";
    $qry = $this->db->prepare($qry);
    $qry->bindParam("post", $args['post']);
    $qry->execute();
    $comments = $qry->fetchAll();
    return $this->response->withJson($comments);
});
