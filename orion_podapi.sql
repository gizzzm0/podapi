-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 25, 2017 at 10:02 PM
-- Server version: 5.7.18
-- PHP Version: 7.0.20-2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `orion_podapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_podposts`
--

CREATE TABLE `tbl_podposts` (
  `pp_id` int(11) UNSIGNED NOT NULL,
  `pp_type` int(1) UNSIGNED NOT NULL,
  `pp_artisteid` int(11) UNSIGNED NOT NULL,
  `pp_title` varchar(255) NOT NULL,
  `pp_sourceURL` varchar(500) DEFAULT NULL,
  `pp_art` varchar(255) DEFAULT NULL,
  `pp_preload` varchar(10) DEFAULT NULL,
  `pp_text` text,
  `pp_dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pp_likes` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `pp_isactive` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_podposts`
--

INSERT INTO `tbl_podposts` (`pp_id`, `pp_type`, `pp_artisteid`, `pp_title`, `pp_sourceURL`, `pp_art`, `pp_preload`, `pp_text`, `pp_dateadded`, `pp_likes`, `pp_isactive`) VALUES
(1, 2, 1, 'Hardwell On Air 2013 Yearmix', 'http://wi.ts-srv.de/music/Hardwell_OnAir/Hardwell%20On%20Air%20Yearmix%202013-Kk1SMMkAE7s.mp3', 'assets/img/art/hoa2013ym.jpg', 'metadata', '<p>I face the unknown future, with a sense of hope. Because if a machine, a Terminator, can learn the value of human life, maybe we can too.</p>', '2017-07-26 00:51:07', 0, 1),
(2, 1, 1, 'Rick and Morty', NULL, 'assets/img/art/rick-and-morty-pic-3.jpg', NULL, '<p>Wait a minute. Wait a minute, Doc. Uhhh... Are you telling me that you built a time machine... out of a DeLorean?! Whoa. This is heavy.</p>', '2017-07-26 00:51:07', 0, 1),
(3, 2, 1, 'The Fall', 'http://akay.co.ke/assets/audio/StadiumXftBISHØP-TheFall.mp3', 'http://akay.co.ke/assets/audio/15850902.jpg', 'metadata', '<p>I face the unknown future, with a sense of hope. Because if a machine, a Terminator, can learn the value of human life, maybe we can too.</p>', '2017-07-26 00:55:09', 0, 1),
(4, 3, 1, 'Big Buck Bunny', 'http://livetargetlures.com/video/big-buck-bunny.mp4', 'http://livetargetlures.com/video/big-buck-bunny.jpg', 'metadata', 'Your scientists were so preoccupied with whether or not they could, that they didn\\\'t stop to think if they should.</p>', '2017-07-26 00:56:11', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_accounts`
--

CREATE TABLE `tbl_user_accounts` (
  `ua_id` int(11) UNSIGNED NOT NULL,
  `ua_name` varchar(255) NOT NULL,
  `ua_username` varchar(255) NOT NULL,
  `ua_email` varchar(255) NOT NULL,
  `ua_password` varchar(255) NOT NULL,
  `ua_tokenchar` varchar(16) DEFAULT NULL,
  `ua_tokenexpiry` datetime DEFAULT NULL,
  `ua_registrationdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ua_avipath` varchar(255) NOT NULL DEFAULT 'assets/img/avis/default.png',
  `ua_coverpath` varchar(255) NOT NULL DEFAULT 'assets/img/covers/default.png',
  `ua_isactive` int(1) UNSIGNED NOT NULL DEFAULT '1',
  `ua_sendnewsletter` int(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_accounts`
--

INSERT INTO `tbl_user_accounts` (`ua_id`, `ua_name`, `ua_username`, `ua_email`, `ua_password`, `ua_tokenchar`, `ua_tokenexpiry`, `ua_registrationdate`, `ua_avipath`, `ua_coverpath`, `ua_isactive`, `ua_sendnewsletter`) VALUES
(1, 'Gideon Muiru', 'yada_yadda', 'm.gid3on@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, NULL, '2017-07-26 01:31:26', 'assets/img/avis/default.png', 'assets/img/covers/default.png', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_podposts`
--
ALTER TABLE `tbl_podposts`
  ADD PRIMARY KEY (`pp_id`);

--
-- Indexes for table `tbl_user_accounts`
--
ALTER TABLE `tbl_user_accounts`
  ADD PRIMARY KEY (`ua_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_podposts`
--
ALTER TABLE `tbl_podposts`
  MODIFY `pp_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_user_accounts`
--
ALTER TABLE `tbl_user_accounts`
  MODIFY `ua_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
